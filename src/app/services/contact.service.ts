import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocialUser } from '../models/user/SocialUser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  BASE_URL: string = "http://localhost:8085/api/v1/contacts";

  constructor(private http: HttpClient , 
    private router: Router){

  }
  
  public findAllContacts(username: string): Observable<any>{
    return this.http.get(this.BASE_URL+"/"+username);
  }
}
