import { Injectable } from '@angular/core';
import { SocialUser } from '../models/user/SocialUser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private BASE_URL: string = 'http://localhost:8080/api/public/v1/auth';

  constructor(private http: HttpClient ,
    private router: Router) {}

  registerUser(user: SocialUser): Observable<any> {
    return this.http.post(this.BASE_URL+"/signup", user);
  }

  loginUser(username , password): Observable<any> {
    return this.http.post(this.BASE_URL+"/login" , 
      {
        "username" : username, 
        "password" : password
      }
    );
  }

  public logoutUser(){
    sessionStorage.setItem("token" , '');
    sessionStorage.setItem("username" , '');
    this.router.navigateByUrl("/login");
  }  
}

