import { Injectable } from '@angular/core';
import {throwError as observableThrowError,  Observable ,  BehaviorSubject } from 'rxjs';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEventType, HttpEvent, HttpErrorResponse, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent } from '@angular/common/http';
import {take, filter, catchError, switchMap, finalize} from 'rxjs/operators';
import { AuthService } from './auth.service';

/**
 * This interceptir wukk add jwt token for every private api.
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService){}

  intercept(request: HttpRequest<any>, next: HttpHandler): 
  Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    let token = sessionStorage.getItem("token");

    if (!request.url.includes('api/public/v1') //if its not a public api
      && token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ` + token
        }
      })
    }

    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
            switch ((<HttpErrorResponse>error).status) {
                case 400:
                case 401:
                  if(!request.url.includes('api/public/v1')){
                    alert("Session Expired / Invalid. \nPlease Login Again");
                    this.authService.logoutUser();
                  }
                  return observableThrowError(error);
            }
        } else {
            return observableThrowError(error);
        }
    })
    );
  }

}