import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { RouterModule } from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { routes } from './route.config';
import {FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './component/home/home.component';
import { TokenInterceptor } from './services/token.interceptor';
import { GallaryComponent } from './component/gallary/gallary.component';
import { ContactsComponent } from './component/contacts/contacts.component';
import { AboutUsComponent } from './component/about-us/about-us.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    GallaryComponent,
    ContactsComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, //required for every http call made through this client.
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes) // saperate routes.config to manage scalablity.
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
