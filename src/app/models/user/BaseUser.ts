import { Authority } from "./Authority";

export class BaseUser {
    public token: string;
    public username: string;
    public password: string;
    public email: string;
    public authorities: Array<Authority> = new Array();
    
}
