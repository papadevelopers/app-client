import { Injectable } from "@angular/core";

@Injectable()
export class AppGlobals {
    public primary_color: String = '#3399ff';
    public app_name: String = "InstaApp";
}
