import { Component, OnInit } from '@angular/core';
import { AppGlobals } from 'src/app/models/app-gobals';
import { AuthService } from '../../services/auth.service'
import { SocialUser } from '../../models/user/SocialUser';
import { Authority } from '../../models/user/Authority';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [AppGlobals]
})
export class RegisterComponent implements OnInit {
  public app_name: String;
  public processing: boolean;
  public hasError: boolean;
  public errorString: string;

  constructor(private globals: AppGlobals,
    private authService: AuthService,
    private router: Router) {
    this.app_name = this.globals.app_name;
  }

  ngOnInit() {
  }

  public register(formData) {
    this.processing = true;
    if(this.validateRegistrationForm(formData.controls)){
      this.authService.registerUser(this.createUser_Form(formData.controls)).subscribe(
          data => { this.completeRegistration();},
          error => {this.addError(error.error.message);}
      )

    }
  }

  validateRegistrationForm(formData): boolean {
    setTimeout(() => { this.addError("") }, 2000);
    
    if(!formData.username.value.match("^[a-zA-Z0-9_]{3,15}[a-zA-Z]+[0-9]*$")){
      this.addError("Invalid username! Username cannot be blank or contain "
          + "(.) or (_) at the start or end!");  
      return false;
    }

    if(!formData.email.value.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")){
      this.addError("Invalid email address!");  
      return false;
    }

    if(!(formData.password.value || formData.confirmPassword.value)){
      this.addError("Passwords cannot be blank!");
      return false; 
    }

    if(!(formData.password.value == formData.confirmPassword.value)){
      this.addError("Passwords do not match!");
      return false;
    }

    return true;
  }

  private completeRegistration(){
    //
    alert("User registered Succssfully!");
    this.router.navigateByUrl("/login");
  }

  createUser_Form(formData): SocialUser {
    let user = new SocialUser();  
    user.email = formData.email.value;
    user.username = formData.username.value;
    user.password = formData.password.value;
    let role = new Authority();
    role.authority = "ROLE_USER"; 
    user.authorities = [role];
    return user;
  }

  /**
   * Usage: It will set error on page if 
   * passed parameter is not null or empty.
   */
  private addError(text: string) {
    if (text) {
      this.hasError = true;
      this.errorString = text;
      this.processing = false;
    } else {
      this.hasError = false;
      this.errorString = "";
    }
  }
}
