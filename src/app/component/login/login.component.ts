import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { SocialUser } from '../../models/user/SocialUser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public processing: boolean;
  public hasError: boolean;
  public errorString: string;

  constructor(private authService: AuthService , 
    private router: Router) { }

  ngOnInit() {
    if(sessionStorage.getItem("token")){
      this.router.navigateByUrl("/home");
    }
  }

  login(formData){
    if(this.validateLoginData(formData.controls)){
        this.processing = true;
        let user = new SocialUser();
        this.authService.loginUser(formData.controls.email.value , formData.controls.password.value).subscribe(
          data=> {
            sessionStorage.setItem("token" , data.token);
            sessionStorage.setItem("username" , data.username);
            this.router.navigateByUrl("/home");
          },  
          error=> {
            alert(error.error.message);
          }
        );
    }
  }

  private validateLoginData(data){
    setTimeout(() => { this.addError("") }, 2000);
    
    if(!data.email.value.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")){
      this.addError("Invalid email address!");  
      return false;
    }

    if(!(data.password.value)){
      this.addError("Password cannot be blank!");
      return false; 
    }
    
    return true;
  }

  /**
   * Usage: It will set error on page if 
   * passed parameter is not null or empty.
   */
  private addError(text: string) {
    if (text) {
      this.hasError = true;
      this.errorString = text;
      this.processing = false;
    } else {
      this.hasError = false;
      this.errorString = "";
    }
  }
}
