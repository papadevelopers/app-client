import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../../services/contact.service';
import { SocialUser } from '../../models/user/SocialUser';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private username: string;
  public contacts: Array<SocialUser> = [];
  private activeChildName: string;

  constructor(private router: Router , 
    private userservice: ContactService,
    private authService: AuthService) {
      this.username = sessionStorage.getItem("username");
    }

  ngOnInit() {
    if(!sessionStorage.getItem("token")){
        this.router.navigateByUrl("/login");
    }else{
      setTimeout(hide=>{
        document.getElementById('preloder').style.display = 'none';
      } , 1200);

      /** fetch all contacts of user */
      this.userservice.findAllContacts(this.username).subscribe(
        data => { this.contacts = data; }
      );

      this.activeChildName = 'gallary';
    }
  }

  public doLogout(){
    document.getElementById('preloder').style.display = 'block';
    setTimeout(log_out=>{
      this.authService.logoutUser();  
    } , 500); 
  }

  public updateChild(childName: string){
    this.activeChildName=childName;
  }
}
